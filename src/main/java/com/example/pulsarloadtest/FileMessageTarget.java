package com.example.pulsarloadtest;

import org.apache.commons.io.FileUtils;
import org.apache.pulsar.client.api.Producer;
import org.apache.pulsar.client.api.PulsarClient;
import org.apache.pulsar.client.api.PulsarClientException;
import org.apache.pulsar.client.api.TypedMessageBuilder;

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

public class FileMessageTarget extends MessageTarget {

    private final String filePath;

    public FileMessageTarget(int partitions, String filePath){
        super(partitions);
        this.filePath = filePath;
    }

    @Override
    public void init(){
    }

    @Override
    void send(int paritionId, Message message) throws Exception{
        FileUtils.writeStringToFile(new File(filePath), new String(message.getValue()) + "\r\n", StandardCharsets.UTF_8, true);
    }
}
