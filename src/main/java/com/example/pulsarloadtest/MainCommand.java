package com.example.pulsarloadtest;

import com.example.pulsarloadtest.file.FileMessageSource;
import com.example.pulsarloadtest.simplerandom.SimpleRandomMessageSource;
import org.springframework.stereotype.Component;
import picocli.CommandLine.Command;
import picocli.CommandLine.Option;

import java.io.IOException;
import java.util.concurrent.Callable;

@Component
@Command(name = "mainCommand", mixinStandardHelpOptions = true)
public class MainCommand implements Callable<Integer> {
    @Option(names = {"--pulsar-host"}, description = "pulsar host")
    private String pulsarHost = "localhost";

    @Option(names = {"--topic"}, description = "pulsar topic")
    private String pulsarTopic = "test";

    @Option(names = {"--source-type"}, description = "source type")
    private String sourceType = "file";

    @Option(names = {"--source-file"}, description = "source file")
    private String sourceFile = "test";

    @Option(names = {"--endpoints"}, description = "num of endpoints")
    private Integer endpointsCount = 100;

    @Option(names = {"--target-type"}, description = "target type")
    private String targetType = "pulsar";

    @Option(names = {"--target-file"}, description = "target file")
    private String targetFile;

    @Option(names = {"--partitions"}, description = "partitions for parallel")
    private Integer partitions = 1;

    @Override
    public Integer call() throws Exception {
        MessageSource messageSource = createSource();
        MessageTarget messageTarget = createTarget();
        TestRunner testRunner = new TestRunner(messageSource, messageTarget, partitions);
        testRunner.start();
        return 0;
    }

    private MessageTarget createTarget() throws Exception{
        if (targetType.equals("pulsar")){
            return new PulsarMessageTarget(partitions, pulsarHost, pulsarTopic);
        } else if (targetType.equals("file")){
            return new FileMessageTarget(partitions, targetFile);
        }
        return new PulsarMessageTarget(partitions, pulsarHost, pulsarTopic);
    }

    private MessageSource createSource() throws IOException {
        if (sourceType.equals("file")) {
            return new FileMessageSource(partitions, sourceFile);
        } else if (sourceType.equals("random")){
            return new SimpleRandomMessageSource(partitions, endpointsCount);
        }
        return new FileMessageSource(partitions, sourceFile);
    }
}