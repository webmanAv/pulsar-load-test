package com.example.pulsarloadtest;

public abstract class MessageTarget {
    protected final int partitions;

    public MessageTarget(int partitions){
        this.partitions = partitions;
    }

    abstract void send(int partitionId, Message message) throws Exception;
    abstract void init() throws Exception;
}
