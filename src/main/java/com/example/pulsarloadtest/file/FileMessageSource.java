package com.example.pulsarloadtest.file;

import com.example.pulsarloadtest.Message;
import com.example.pulsarloadtest.MessageSource;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.LineIterator;

import java.io.File;
import java.io.IOException;

public class FileMessageSource extends MessageSource {
    private final String filePath;
    private LineIterator it;
    public FileMessageSource(int partitions, String filePath) throws IOException {
        super(partitions);
        this.filePath = filePath;
        this.it = FileUtils.lineIterator(new File(filePath), "UTF-8");
    }

    @Override
    public Message nextMessage(int partitionIndex) {
        if (it.hasNext()){
            Message message = new Message();
            String msgString = it.nextLine();
            if (msgString != null) {
                message.setValue(msgString.getBytes());
                return message;
            }
        }
        return null;
    }
}
