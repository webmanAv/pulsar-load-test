package com.example.pulsarloadtest;
public abstract class MessageSource{
    protected final int partitions;

    public MessageSource(int partitions){
        this.partitions = partitions;
    }

    public abstract Message nextMessage(int partitionIndex);
}


