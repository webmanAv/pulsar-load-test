package com.example.pulsarloadtest.simplerandom;

import net.andreinc.mockneat.MockNeat;

public abstract class PluginBase {
    protected final Network network;
    protected MockNeat mock = MockNeat.threadLocal();
    public PluginBase(Network network) {
        this.network = network;
    }

    abstract public LearnEvent nextLearnEvent();

    protected void putKey(Interface inter, LearnEvent learnEvent) {
        boolean hasKey = false;
        if (mock.bools().probability(70).get()){
            learnEvent.putIpv4(inter.getIpv4());
            hasKey = true;
        }

        if (mock.bools().probability(20).get()){
            learnEvent.putIpv6(inter.getIpv6());
            hasKey = true;
        }

        if (mock.bools().probability(20).get()){
            learnEvent.putMac(inter.getMac());
            hasKey = true;
        }

        if (!hasKey){
            learnEvent.putIpv4(inter.getIpv4());
        }
    }
}
