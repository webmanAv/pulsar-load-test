package com.example.pulsarloadtest.simplerandom;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Endpoint {
    private String hostName;
    private List<Interface> interfaces = new ArrayList<>();
    private String deviceType;
}
