package com.example.pulsarloadtest.simplerandom;

import lombok.Data;

import java.util.HashMap;
import java.util.Map;

@Data
public class LearnEvent {
    private Map<String, Object> map = new HashMap<>();
    public void put(String key, Object value){
        map.put(key, value);
    }

    public void putIpv4(String ip){
        map.put("ipv4", ip);
    }

    public void putIpv6(String ip){
        map.put("ipv6", ip);
    }

    public void putMac(String mac){
        map.put("mac", mac);
    }

    public void putHostName(String hostName){
        map.put("hostname", hostName);
    }
}
