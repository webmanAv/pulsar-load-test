package com.example.pulsarloadtest.simplerandom;

import com.example.pulsarloadtest.Message;
import com.example.pulsarloadtest.MessageSource;
import lombok.Data;
import org.apache.pulsar.shade.org.codehaus.jackson.map.ObjectMapper;

import java.io.IOException;
import java.util.*;

public class SimpleRandomMessageSource extends MessageSource {
    private ObjectMapper objectMapper = new ObjectMapper();
    private List<NetworkWrapper> networks = new ArrayList<>();

    public SimpleRandomMessageSource(int partitions, int endpointsCount){
        super(partitions);
        for (int i=0; i<partitions; i++){
            Network network = new Network(endpointsCount);
            NetworkWrapper networkWrapper = new NetworkWrapper();
            networkWrapper.setCounter(0L);
            networkWrapper.setPlugins(Arrays.asList(new Plugin1(network), new Plugin2(network)));
            networks.add(i, networkWrapper);
        }
    }

    @Override
    public Message nextMessage(int partitionIndex) {
        List<PluginBase> plugins = networks.get(partitionIndex).getPlugins();
        Long counter = networks.get(partitionIndex).getCounter();

        int currentPluginIndex = (int) (counter % plugins.size());
        LearnEvent learnEvent = plugins.get(currentPluginIndex).nextLearnEvent();
        networks.get(partitionIndex).setCounter(counter + 1);
        try {
            Message message = new Message();
            message.setValue(objectMapper.writeValueAsBytes(learnEvent.getMap()));
            return message;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Data
    static class NetworkWrapper{
        private List<PluginBase> plugins;
        private Long counter;
    }
}
