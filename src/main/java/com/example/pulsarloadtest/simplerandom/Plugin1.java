package com.example.pulsarloadtest.simplerandom;

public class Plugin1 extends PluginBase
{
    public Plugin1(Network network){
        super(network);
    }

    @Override
    public LearnEvent nextLearnEvent(){
        Interface inter = this.network.randomInterface();

        LearnEvent learnEvent = new LearnEvent();
        for (int i=0; i<3; i++) {
            learnEvent.put("propA" + i, mock.strings().size(10).get());
        }
        putKey(inter, learnEvent);

        return learnEvent;
    }
}
