package com.example.pulsarloadtest.simplerandom;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Interface {
    private String ipv4;
    private String mac;
    private String ipv6;
    private Endpoint parent;
}
