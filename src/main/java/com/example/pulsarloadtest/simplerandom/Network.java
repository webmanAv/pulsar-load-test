package com.example.pulsarloadtest.simplerandom;

import net.andreinc.mockneat.MockNeat;
import net.andreinc.mockneat.types.enums.IPv4Type;

public class Network{
    private Endpoint[] endpoints;
    private MockNeat mock;

    public Network(int endpointsCount){
        this.endpoints = new Endpoint[endpointsCount];
        this.mock = MockNeat.threadLocal();
        for (int i=0; i<endpointsCount; i++){
            this.endpoints[i] = createEndpoint("host" + i);
        }
    }

    private Endpoint createEndpoint(String hostName){
        Endpoint endpoint = new Endpoint();
        endpoint.setHostName(hostName);
        Interface inter1 = createInterface();
        Interface inter2 = createInterface();
        inter1.setParent(endpoint);
        inter2.setParent(endpoint);
        endpoint.getInterfaces().add(inter1);
        endpoint.getInterfaces().add(inter2);
        return endpoint;
    }

    private Interface createInterface(){
        Interface inter = new Interface();
        inter.setIpv4(mock.ipv4s().type(IPv4Type.CLASS_A_NONPRIVATE).get());
        inter.setIpv6(mock.iPv6s().get());
        inter.setMac(mock.macs().get());
        return inter;
    }

    public Endpoint randomEndpoint(){
        int endpointIndex = this.mock.ints().range(0, endpoints.length).get();
        return endpoints[endpointIndex];
    }

    public Interface randomInterface(){
        Endpoint endpoint = randomEndpoint();
        int interfaceCount = endpoint.getInterfaces().size();
        int index = this.mock.ints().range(0, interfaceCount).get();
        return endpoint.getInterfaces().get(index);
    }
}
