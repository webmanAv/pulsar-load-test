package com.example.pulsarloadtest;

import org.apache.pulsar.client.api.Producer;
import org.apache.pulsar.client.api.PulsarClient;
import org.apache.pulsar.client.api.PulsarClientException;
import org.apache.pulsar.client.api.TypedMessageBuilder;

import java.util.ArrayList;
import java.util.List;

public class PulsarMessageTarget extends MessageTarget {
    private String topic;
    private String pulsarHost;
    private PulsarClient pulsarClient;
    private List<Producer<byte[]>> producers = new ArrayList<>();

    public PulsarMessageTarget(int partitions, String pulsarHost, String topic){
        super(partitions);
        this.pulsarHost = pulsarHost;
        this.topic = topic;
    }

    @Override
    public void init() throws PulsarClientException {
        this.pulsarClient = createPulsarClient(pulsarHost);
        for (int i=0; i<partitions; i++) {
            producers.add(i, createPulsarProducer());
        }
    }

    @Override
    void send(int partitionId, Message message) throws Exception{
        Producer<byte[]> producer = producers.get(partitionId);
        TypedMessageBuilder<byte[]> typedMessageBuilder = producer.newMessage();
        if (message.getKey() != null){
            typedMessageBuilder = typedMessageBuilder.key(message.getKey());
        }
        typedMessageBuilder.value(message.getValue()).send();
    }

    private Producer<byte[]> createPulsarProducer() throws PulsarClientException {
        return pulsarClient.newProducer().enableBatching(false)
                .topic(topic).create();
    }

    public PulsarClient createPulsarClient(String pulsarHost) throws PulsarClientException {
        return PulsarClient.builder()
                .serviceUrl("pulsar://"+ pulsarHost + ":6650")
                .build();
    }
}
