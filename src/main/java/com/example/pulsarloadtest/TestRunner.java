package com.example.pulsarloadtest;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicLong;

public class TestRunner {
    private final MessageSource messageSource;
    private final MessageTarget messageTarget;
    private final int partitions;
    private ExecutorService executorService;
    Monitor monitor = new Monitor();
    List<Future> futureList = new ArrayList<>();

    static class Monitor{
        public AtomicLong generateDuration = new AtomicLong();
        public AtomicLong writeDuration = new AtomicLong();
        public AtomicLong writeCount = new AtomicLong();
        public AtomicLong writeBytes = new AtomicLong();

        public void reset(){
            writeDuration.set(0);
            generateDuration.set(0);
            writeCount.set(0);
            writeBytes.set(0);
        }

        public void print(){
            System.out.println(String.format("%d write per/second, %d kb/s, overhead %5.2f%%", writeCount.get(), writeBytes.get()/1024,  (double)generateDuration.get()/10));
        }
    }

    public TestRunner(MessageSource messageSource, MessageTarget messageTarget, int partitions){
        this.messageSource = messageSource;
        this.messageTarget = messageTarget;
        this.executorService = Executors.newFixedThreadPool(1 + partitions);
        this.partitions = partitions;
    }

    public void start() throws Exception {
        this.messageTarget.init();
        this.executorService.submit(()->{
            try {
                while(!Thread.currentThread().isInterrupted()) {
                    Thread.sleep(1000);
                    monitor.print();
                    monitor.reset();
                }
            }catch (InterruptedException e) {
                e.printStackTrace();
            }
        });

        for (int i=0; i<partitions; i++) {
            int finalI = i;
            futureList.add(i,
                this.executorService.submit(() -> {
                    while (!Thread.currentThread().isInterrupted()) {
                        try {
                            produce(finalI);
                        } catch (Exception e) {
                            e.printStackTrace();
                            break;
                        }
                    }
                }));
        }

        for (int i=0; i<partitions; i++){
            futureList.get(i).get();
        }

    }

    private void produce(int partitionId) throws Exception {
        while(!Thread.currentThread().isInterrupted()){
            Message message = nextMessage(partitionId);
            if (message == null){
                break;
            }
            long before = System.currentTimeMillis();
            messageTarget.send(partitionId, message);
            monitor.writeCount.incrementAndGet();
            monitor.writeDuration.addAndGet(System.currentTimeMillis()-before);
            monitor.writeBytes.addAndGet(message.getValue().length);
        }
    }

    private Message nextMessage(int partition) {
        long before = System.currentTimeMillis();
        Message message = messageSource.nextMessage(partition);
        monitor.generateDuration.addAndGet(System.currentTimeMillis()-before);
        return message;
    }
}

