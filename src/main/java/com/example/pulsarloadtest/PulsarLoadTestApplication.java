package com.example.pulsarloadtest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PulsarLoadTestApplication {
	public static void main(String[] args) {
		System.exit(SpringApplication.exit(SpringApplication.run(PulsarLoadTestApplication.class, args)));
	}
}
