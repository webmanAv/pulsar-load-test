#!/bin/bash
docker run -d \
  -p 6650:6650 \
  --mount source=pulsardata,target=/pulsar/data \
  --mount source=pulsarconf,target=/pulsar/conf \
  --name pulsar \
  apachepulsar/pulsar:2.5.0 \
  bin/pulsar standalone


